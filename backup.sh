#!/usr/bin/env bash
# shellcheck disable=SC2016

TIMESTAMP=$(date + '%Y-%m-%d_%H%M%S')

printf "Backing up NetBox database\\n"
docker-compose exec postgres sh -c 'pg_dump -cU $POSTGRES_USER $POSTGRES_DB' | gzip > backups/db_dump_"$TIMESTAMP".sql.gz

printf "Backing up NetBox media\\n"
docker-compose exec -T netbox tar c -jf - -C /opt/netbox/netbox/media ./ > backups/media-backup_"$TIMESTAMP".tar.bz2
